/*!
 * Project:   PIKA
 * Date:      YYYY/MM/DD
 * Author:    Maciej Węgrzynek <maciej@hitmo-studio.com>, hitmo-studio.com
/* ---------------------------------------- */

/*!
 * Some older (but still brilliant) plugins need support of this function
 * jQuery Browser Support - v1.0.0 - 16.01.2013
 * https://github.com/SerafimArts/jquery.browser.js
 * Copyright 2013 Serafim Arts, Inc. and other contributors; Licensed MIT
 */
;(function(e){e.uaMatch=function(e){e=e.toLowerCase();var t=/(chrome)[ \/]([\w.]+)/.exec(e)||/(webkit)[ \/]([\w.]+)/.exec(e)||/(opera)(?:.*version|)[ \/]([\w.]+)/.exec(e)||/(msie) ([\w.]+)/.exec(e)||e.indexOf("compatible")<0&&/(mozilla)(?:.*? rv:([\w.]+)|)/.exec(e)||[];return{browser:t[1]||"",version:t[2]||"0"}};matched=e.uaMatch(navigator.userAgent);browser={};if(matched.browser){browser[matched.browser]=true;browser.version=matched.version}if(browser.chrome){browser.webkit=true}else if(browser.webkit){browser.safari=true}e.browser=browser})(jQuery);

/*!
 * Scripts
 *
 */
var dev = true;
// depending on environment this is set to false on production and true on any other env
// so console alerts are not visible on production
dev && window.console && console.log(dev);

var Engine = Engine || {};

Engine.createNS = function (namespace) {
    var nsparts = namespace.split(".");
    var parent = Engine;

    // we want to be able to include or exclude the root namespace
    // So we strip it if it's in the namespace
    if (nsparts[0] === "Engine") {
        nsparts = nsparts.slice(1);
    }

    // loop through the parts and create
    // a nested namespace if necessary
    for (var i = 0; i < nsparts.length; i++) {
        var partname = nsparts[i];
        // check if the current parent already has
        // the namespace declared, if not create it
        if (typeof parent[partname] === "undefined") {
            parent[partname] = {};
        }
        // get a reference to the deepest element
        // in the hierarchy so far
        parent = parent[partname];
    }
    // the parent is now completely constructed
    // with empty namespaces and can be used.
    return parent;
};

Engine.createNS("Engine.mainObjLib");
Engine.createNS("Engine.ui");
Engine.createNS("Engine.utils");
Engine.createNS("Engine.ajax");


Engine.mainObjLib = {
	body : $('body'),
	top : $('#top'),
	footer : $('#footer'),
	content : $('#content'),
	window : $(window),
	document : $(document)
}

Engine.utils = {
	links : function(){
		$('a[rel*="external"]').click(function(e){
			e.preventDefault();
			window.open($(this).attr('href'));
		});
	}
}

Engine.ui = {
	screen : {
		objLib : {
			scrollbarWidth : 0,
			screenPos : 0
		},
		init : function () {
			var self = this;
			self.getScrollbarWidth();
		},
		lockScreen : function (callback) {
			var self = this;
			self.lockScroll();
			Engine.mainObjLib.body.addClass('overlayed');
			if (self.checkIfSetBodyMargin()) {
				Engine.console(self.objLib.scrollbarWidth);
				Engine.mainObjLib.body.css({ 'margin-right' : self.objLib.scrollbarWidth });
				if ($('.nav-wrapper').hasClass('fixed')) {
					$('.nav-wrapper, .box-e').css({ 'padding-right' : self.objLib.scrollbarWidth });
				}
			}
			if (typeof callback == 'function') {
				callback();
			}
		},
		unlockScreen : function (callback) {
			var self = this;
			self.unlockScroll();
			Engine.mainObjLib.body.css({ 'margin-right' : 0 });
			Engine.mainObjLib.body.removeClass('overlayed');
			if (typeof callback == 'function') {
				callback();
			}
		},
		getScrollbarWidth : function () {
			var self = this;
			// http://benalman.com/projects/jquery-misc-plugins/#scrollbarwidth
			var parent = $('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo('body');
			var child  = parent.children();
			self.objLib.scrollbarWidth = child.innerWidth() - child.height( 99 ).innerWidth();
			parent.remove();
		},
		checkIfSetBodyMargin : function () {
			if (Engine.mainObjLib.window.height() < Engine.mainObjLib.document.height()) {
				return true;
			}
			return false;
		},
		lockScroll : function () {
			var self = this;
			self.objLib.screenPos = Engine.mainObjLib.window.scrollTop();
			Engine.mainObjLib.window.on('scroll.locked', function (e) {
				e.preventDefault();
				Engine.mainObjLib.window.scrollTop(self.objLib.screenPos);
			});
		},
		unlockScroll : function () {
			var self = this;
			Engine.mainObjLib.window.unbind('scroll.locked');
		}
	}
}

Engine.ajax = {
	sendRequest : function (url, post, callback) {
		var self = this;
		$.ajax({
			url: url,
			type: "POST",
			data: post,
			success: function(data) {
				self.getRespond(data, callback);
			}
		});
	},
	getRespond : function (data, callback) {
		var self = this;
		if (typeof data != 'object') {
			var data = jQuery.parseJSON(data);
		}
		callback(data);
	}
}


jQuery(function($) {

	Engine.utils.links();
	Engine.ui.screen.init();

});